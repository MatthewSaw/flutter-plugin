# eGHL Flutter Plugin

Example of Flutter integration with eGHL SDK

Linking Libraries
---------------------------------

### Importing .aar file

**Step 1**
Create a libs folder in android, copy the aar file into the folder

![picture](images/aarFileImport.png)

**Step 2**
Open build.gradle in android folder

implement the SDK `.aar` file inside of a dependencies column

````
dependencies {
    implementation files('./eghl-sdk/eghl-sdk-v2.4.7.aar')
}
````

**Step 3**
Open the settings.gradle in android folder, include the following line

````
include ':eghl-sdk'
````

Usage
------

### Create channel between Flutter and Android

In the ../lib folder open `eghl_flutter.dart` file, create a function that will link application to plugin

```
static Future<String> executePayment(Map<String, dynamic> params) async {
    final String result = await _channel.invokeMethod('executePayment', params);
    return result;
  }
```

In android/src/main/java/com/ghl/eghl/eghl_flutter/ create a Payment Parameter class that accepts all parameters required for a payment transaction, example shown below

```
 @SerializedName("TransactionType")
 private String transactionType = "";

 public String getTransactionType() {
    return transactionType;
 }

 public void setTransactionType(String transactionType) {
    this.transactionType = transactionType;
 }

```

and utilize the EghlFlutterPlugin class to handle the payment request and result

**Payment request handler**
```
@Override
public void onMethodCall(MethodCall call, Result result) {
    
    if(call.method.equals("executePayment")) {
        //Build payment parameters

        Intent payment = new Intent(this._activity, PaymentActivity.class);
        payment.putExtras(params.build());

        isInProgress = true;
        this._activity.startActivityForResult(payment, EGHL.REQUEST_PAYMENT)
    }
}
```

**Result handler**
```
 @Override
  public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
    if (requestCode == EGHL.REQUEST_PAYMENT) {
      isInProgress = false;
      String message = data.getStringExtra(EGHL.TXN_MESSAGE);
      switch (resultCode) {
        case EGHL.TRANSACTION_SUCCESS:
          Log.d(TAG, "onActivityResult: payment successful");
          String resultString = data.getStringExtra(EGHL.RAW_RESPONSE);
          this._result.success(resultString);
          this._result = null;

          break;
        case EGHL.TRANSACTION_FAILED:
          if(message == null) {
            Log.d(TAG, "onActivityResult: payment failure");
            this._result.error("PAYMENT_FAILED", String.valueOf(resultCode), null);
            this._result = null;
          } else {
            // Check for "buyer cancelled" string in JS
            Log.d(TAG, "onActivityResult: payment failure or cancelled '"+message+"'");
            this._result.error("PAYMENT_CANCELLED", message, null);
            this._result = null;
          }

          break;
        default:
          Log.d(TAG, "onActivityResult: " + resultCode);
          if(message == null) {
            this._result.error("UNKNOWN_FAILED", String.valueOf(resultCode), null);
            this._result = null;
          } else {
            this._result.error("UNKNOWN_CANCELLED", message, null);
          }

          break;
      }
    }
    return false;
  }
```

### Request Payment

```
import 'package:eghl_flutter/eghl_flutter.dart';
```

Build the payment params in a Map<String, dynamic> in paymentPage.dart

```
String result = '';
String paymentId = 'SIT${new DateTime.now().millisecondsSinceEpoch}';

Map<String, dynamic> payment = {
        'TransactionType': 'SALE',
        'Amount': '1.00',
        'CurrencyCode': 'MYR',
        'PaymentID': paymentId,
        'OrderNumber': paymentId,
        'PaymentDesc': 'Testing Payment',
        'PymtMethod': 'ANY',

        'CustName': 'somebody',
        'CustEmail': 'somebody@someone.com',
        'CustPhone': '0123456789',

        'MerchantReturnURL': 'SDK',
        'MerchantCallbackURL': 'SDK',

        'ServiceID': 'SIT',
        'Password': 'sit12345',
        'LanguageCode': 'EN',
        'PageTimeout': '600',
        'Production' : false,
      };

result = await EghlFlutter.executePayment(payment);

```

