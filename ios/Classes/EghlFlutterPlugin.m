#import "EghlFlutterPlugin.h"
#import <eghl_flutter/eghl_flutter-Swift.h>

@implementation EghlFlutterPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftEghlFlutterPlugin registerWithRegistrar:registrar];
}
@end
