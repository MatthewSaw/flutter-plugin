import 'dart:async';

import 'package:flutter/services.dart';

class EghlFlutter {
  static const MethodChannel _channel =
      const MethodChannel('eghl_flutter');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String> executePayment(Map<String, dynamic> params) async {
    final String result = await _channel.invokeMethod('executePayment', params);
    return result;
  }
}
